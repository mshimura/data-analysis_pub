# -*- coding: utf-8 -*-
"""
Created on Sun Aug  2 15:12:24 2020
testとtrainを違うcsvにする
@author: mshim
"""

import pandas as pd
import numpy as np
from sklearn import linear_model
import matplotlib.pyplot as plt

df=pd.read_csv("1.N_oil_f.csv")
train=df[:250]
test=df[250:]
y_train=train["4std_friction"]
X_train=train.drop("4std_friction",axis=1)
X_test=test.drop("4std_friction",axis=1)
y_test=test["4std_friction"]
print(len(y_train))
print(len(X_train))


model=linear_model.LinearRegression()
model.fit(X_train, y_train)
#print(model.coef_)
#print(model.score(X_test, y_test))
print(len(model.predict(X_train)))

fig=plt.figure()
ax1 = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)
data=np.arange(0,250,1)
y1=y_test
y2=model.predict(X_test)
ax1.plot(data, y1, color="blue", label="y_test")
ax2.plot(data, y2, color="orange", label="y_predict")
#plt.plot(model.predict(X_test))
#plt.plot(y_test)
# plt.scatter(X_train,model.predict(X_train))
fig.tight_layout()
plt.show()